# About 

Repository for an example Gridsome project that uses Netlify, NetlifyCMS and GitLab to host a static rendered blog.

View the post on [drewtown.dev](https://www.drewtown.dev/post/setting-up-gridsome-with-gitlab-netlifycms-and-netlify/)


### 1. Install Gridsome CLI tool if you don't have

`npm install --global @gridsome/cli`

### 2. Run the site in development mode

`gridsome develop`

### 3. Browse to the site

`http://localhost:8080/`

